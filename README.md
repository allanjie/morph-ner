## Modeling Morphology in Named Entity Recognition

The code consists of two types of systems: baseline model, which is an LSTM-CRF model and another one is latent LSTM-CRF.

### Requirements
* Dynet 2.0

Put your datasets under a data folder. In this repo, we have four datasets to use: CoNLL-2003 English, German and CoNLL-2002 Spanish and Dutch.
You need to strictly follow the following directory structure as well as the naming (unless you change them in the code). 
```
| -- data
|    |-- english
        `-- train.txt
         `-- dev.txt
         `-- test.txt
|    |-- german
         `-- train.txt
         `-- dev.txt
         `-- test.txt
|    |-- spanish
         `-- train.txt
         `-- dev.txt
         `-- test.txt
|    |-- dutch
         `-- train.txt
         `-- dev.txt
         `-- test.txt
     `-- english.embeddings.txt
     `-- german.embeddings.txt
     `-- spanish.embeddings.txt
     `-- dutch.embeddings.txt
```

As our code will save the models, you need to create a `models` folder. Otherwise, it would report errors.

### Run the Baseline LSTM-CRF model
You can find `main` entry in `morph-ner-main.py`. You can simply run the following command
```bash
    python3 morph-ner-main.py --dataset english --model_type baseline
```
We have two model types: `baseline` or `standard`. `baseline` is the same neural architecture as in Lample et al., 2016. `standard` is the one we discuss.


### Running the character-based model (baseline)

You can find `main` entry in `benchmark-main.py`. You can simply run the following command
```bash
    python3 morph-ner-main.py --model_type standard --dataset english
```


### Others
You can find other options/arguments in the main file to modify. **Additionally, we provide two scripts for you to run these two systems easily:** `run-benchmark.bash` and `run-standard.bash`.
