#!/bin/bash

optimizer=adam
lr=0.1
batch=1
eval_epoch=0
seed=42
model_type=standard
hidden_dim=200
train_num=-1
num_epoch=50
datasets=(english spanish german dutch)


for (( d=0; d<${#datasets[@]}; d++ )) do
  dataset=${datasets[$d]}
  python3 morph-ner-main.py --dynet-seed ${seed}  --model_type ${model_type} --eval_freq 20000 \
         --optimizer ${optimizer} --learning_rate ${lr} --batch_size ${batch} --train_num ${train_num} \
         --eval_epoch ${eval_epoch} --dataset ${dataset} \
         --hidden_dim ${hidden_dim}  > logs/${dataset}_${model_type}_${train_num}_${optimizer}_${lr}_batch_${batch}_hid_${hidden_dim}.log 2>&1 &
done



