import dynet as dy
from utils import log_sum_exp
import numpy as np
from char_rnn import CharRNN
from utils import check_bies_constraint

START = "<START>"
STOP = "<STOP>"

import time

class Standard_BiLSTM_CRF:

    def __init__(self, config, model, load_param=False):
        self.config = config

        self.num_layers = 1
        self.input_dim = config.embedding_dim
        self.model = model
        self.use_char_rnn = config.use_char_rnn
        input_size = self.input_dim if not self.use_char_rnn else self.input_dim + config.charlstm_hidden_dim

        input_size = input_size if self.config.concat_char_to_word else input_size - config.charlstm_hidden_dim
        print("Input to word-level BiLSTM size: %d" % (input_size))
        print("BiLSTM hidden size: %d" % (config.hidden_dim))

        self.num_labels = len(config.label2idx)
        self.num_entity_labels = len(config.ent2idx)
        self.label2idx = config.label2idx
        self.labels = config.idx2labels

        self.ent2idx = config.ent2idx
        self.entity = config.idx2entity

        self.label_idx2ent_idx = config.label_idx2ent_idx
        vocab_size = len(config.word2idx)
        self.word2idx = config.word2idx
        print("Word Embedding size: %d x %d" % (vocab_size, self.input_dim))


        if load_param:
            print("[Info] Loading pretrained parameters")
            char_emb, fw_lstm, bw_lstm, self.word_embedding, self.bilstm = dy.load("basename", self.model)
            self.char_rnn = CharRNN(config, model, load_param,char_emb, fw_lstm,bw_lstm) if self.use_char_rnn else None
        else:
            self.char_rnn = CharRNN(config, model, load_param) if self.use_char_rnn else None
            self.bilstm = dy.BiRNNBuilder(1, input_size, config.hidden_dim, self.model, dy.LSTMBuilder)
            self.word_embedding = self.model.add_lookup_parameters((vocab_size, self.input_dim),
                                                                   init=config.word_embedding)



        # print(config.hidden_dim)

        self.linear_w = self.model.add_parameters((self.num_labels, config.hidden_dim))
        self.linear_bias = self.model.add_parameters((self.num_labels,))

        self.char_linear_w = self.model.add_parameters((self.num_labels, config.charlstm_hidden_dim))
        self.char_linear_bias = self.model.add_parameters((self.num_labels,))

        self.transition = self.model.add_lookup_parameters((self.num_labels, self.num_labels))

        mask_mat = []  ### mask for positions in a word.
        for l1 in range(self.num_labels):
            label_1 = self.labels[l1]
            mat = []
            for l2 in range(self.num_labels):
                label_2 = self.labels[l2]
                ## l2 -> l1 transition.
                if label_1 == config.O:
                    mat.append(0 if label_2 == label_1 else -1e10)
                elif label_1 == config.START_TAG or label_1 == config.STOP_TAG:
                    mat.append(-1e10)
                    # after this, label 1 should start with B,I,E,S
                elif label_2 == config.STOP_TAG or label_2 == config.START_TAG or label_2 == config.O:
                    mat.append(-1e10)
                    # after this, both l1 and l2 start with B, I, E, S
                elif label_2[:-2] != label_1[:-2]:
                    mat.append(-1e10)
                else:
                    mat.append(0 if check_bies_constraint(config, label_2[-1], label_1[-1]) else -1e10)
            mask_mat.append(mat)
        # self.mask = self.model.add_lookup_parameters((self.num_labels, self.num_labels), init=np.array(mask_mat))
        # self.mask = dy.inputTensor(mask_mat)
        self.mask_mat = mask_mat
        # self.mask.set_updated(False)
        self.dropout = config.dropout



    def build_graph_with_char(self, x, all_chars, is_train):
        embeddings = []
        all_char_feats = []
        for w, chars in zip(x, all_chars):
            word_emb = self.word_embedding[w]
            f, b = self.char_rnn.forward_char(chars)
            concat = dy.concatenate([word_emb, f[-1], b[-1]]) if self.config.concat_char_to_word else word_emb

            embeddings.append(dy.dropout(concat, self.dropout) if is_train else concat)

            char_rep = [dy.concatenate([hf, hb]) for hf, hb in zip(f, b[::-1])]
            char_feats = [dy.affine_transform([self.char_linear_bias, self.char_linear_w, rep]) for rep in char_rep]
            all_char_feats.append(char_feats)
        lstm_out = self.bilstm.transduce(embeddings)
        # print(len(all_char_feats))
        # print(all_char_feats[0][0].dim())
        # tanh_feats = [dy.tanh(dy.affine_transform([self.tanh_bias, self.tanh_w, rep])) for rep in lstm_out]
        features = [dy.affine_transform([self.linear_bias, self.linear_w, rep]) for rep in lstm_out]
        return features, all_char_feats

    def forward_unlabeled(self, features, all_char_feats, x_chars, mask):
        init_alphas = [-1e10] * self.num_labels
        init_alphas[self.label2idx[START]] = 0

        for_expr = dy.inputVector(init_alphas)
        # print(len(all_char_feats))
        for i in range(len(x_chars)):
            char_feats = all_char_feats[i]
            # print("\t" + str(len(char_feats)))
            for j in range(len(x_chars[i])): ##compute the jth score
                alphas_t = []
                char_f = char_feats[j]
                for next_tag in range(self.num_labels):
                    obs_broadcast = dy.concatenate([dy.pick(char_f, next_tag)] * self.num_labels)
                    next_tag_expr = for_expr + self.transition[next_tag] + obs_broadcast
                    # if next_tag == self.label2idx[START] or next_tag == self.label2idx[STOP]:
                    #     alphas_t.append(dy.scalarInput(-1e10))
                    # else:
                    # next_tag_expr = for_expr
                    if j == 0:
                        w_broadcast = dy.concatenate([dy.pick(features[i], next_tag)] * self.num_labels)
                        next_tag_expr += w_broadcast
                    else:
                        next_tag_expr += mask[next_tag] ##make sure same entity
                    alphas_t.append(log_sum_exp(next_tag_expr, self.num_labels))
                for_expr = dy.concatenate(alphas_t)

        terminal_expr = for_expr + self.transition[self.label2idx[STOP]]
        alpha = log_sum_exp(terminal_expr, self.num_labels)
        return alpha

    # Labeled network score
    def forward_labeled(self, features, all_char_feats, possible_tags, x_chars, mask):

        init_alphas = [-1e10] * self.num_labels
        init_alphas[self.label2idx[START]] = 0

        for_expr = dy.inputVector(init_alphas)

        for i in range(len(x_chars)):
            char_feats = all_char_feats[i]
            # print(possible_tags)
            # print([self.labels[x] for x in possible_tags[i]  ])
            for j in range(len(x_chars[i])):  ##compute the jth score
                alphas_t = []
                char_f = char_feats[j]
                for next_tag in range(self.num_labels):
                    if next_tag in possible_tags[i]:
                        obs_broadcast = dy.concatenate([dy.pick(char_f, next_tag)] * self.num_labels)
                        next_tag_expr = for_expr + self.transition[next_tag] + obs_broadcast
                        if j == 0:
                            w_broadcast = dy.concatenate([dy.pick(features[i], next_tag)] * self.num_labels)
                            next_tag_expr += w_broadcast
                        else:
                            next_tag_expr += mask[next_tag] ##make sure same entity
                        alphas_t.append(log_sum_exp(next_tag_expr, self.num_labels))
                    else:
                        alphas_t.append( dy.scalarInput(-1e10))
                for_expr = dy.concatenate(alphas_t)

        terminal_expr = for_expr + self.transition[self.label2idx[STOP]]
        alpha = log_sum_exp(terminal_expr, self.num_labels)
        # print(alpha.npvalue())
        return alpha


    def negative_log(self, x, x_chars, possible_tags): ## possible tags at each position
        start_time = time.time()
        features, all_char_feats = self.build_graph_with_char(x,x_chars,True)
        # features = self.build_graph(x, True)

        mask = dy.inputTensor(self.mask_mat)
        # build_trans_time = time.time()
        unlabed_score = self.forward_unlabeled(features, all_char_feats, x_chars, mask)
        # unlabel_time = time.time()
        labeled_score = self.forward_labeled(features, all_char_feats, possible_tags,x_chars, mask)
        # label_time = time.time()
        # print("all time: %.2f, lstm time: %.2f, trans time: %.2f, unlabeled: %.2f, labeled: %.2f"
        #       % (label_time - start_time, lstm_time-start_time, build_trans_time-lstm_time, unlabel_time-build_trans_time,
        #          label_time-unlabel_time))
        # print(unlabed_score.value())
        # print(labeled_score.value())
        return unlabed_score - labeled_score

    def viterbi_decoding(self, features, all_char_feats, x_chars, mask):
        backpointers = []
        init_vvars = [-1e10] * self.num_labels
        init_vvars[self.label2idx[START]] = 0  # <Start> has all the probability
        for_expr = dy.inputVector(init_vvars)
        trans_exprs = [self.transition[idx] for idx in range(self.num_labels)]

        for i in range(len(x_chars)):
            char_feats = all_char_feats[i]
            # print(i)
            for j in range(len(x_chars[i])):
                char_f = char_feats[j]
                # print("\t" + str(j))
                bptrs_t = []
                vvars_t = []
                for next_tag in range(self.num_labels):
                    next_tag_expr = for_expr + trans_exprs[next_tag]
                    if j != 0:
                        next_tag_expr += mask[next_tag]
                    next_tag_arr = next_tag_expr.npvalue()
                    best_tag_id = np.argmax(next_tag_arr) ## the best tag id to next tag
                    bptrs_t.append(best_tag_id)
                    vvars_t.append(dy.pick(next_tag_expr, best_tag_id))
                # for_expr = dy.concatenate(vvars_t) + all_char_feats[i][j]
                for_expr = dy.concatenate(vvars_t) + char_f
                if j == 0:
                    for_expr += features[i]
                backpointers.append(bptrs_t)
        terminal_expr = for_expr + trans_exprs[self.label2idx[STOP]]
        terminal_arr = terminal_expr.npvalue()
        best_tag_id = np.argmax(terminal_arr)
        path_score = dy.pick(terminal_expr, best_tag_id)
        # Reverse over the backpointers to get the best path
        best_path = [best_tag_id]  # Start with the tag that was best for terminal
        for bptrs_t in reversed(backpointers):
            best_tag_id = bptrs_t[best_tag_id]
            best_path.append(best_tag_id)

        start = best_path.pop()  # Remove the start symbol
        best_path.reverse()
        # print(best_path)

        assert start == self.label2idx[START]
        # Return best path and best path's score
        return best_path, path_score

    def decode(self, x, x_chars=None):
        features, all_char_feats = self.build_graph_with_char(x,x_chars,False)
        # features = self.build_graph(x, False)
        mask = dy.inputTensor(self.mask_mat)
        best_path, path_score = self.viterbi_decoding(features, all_char_feats, x_chars, mask)
        best_path = [self.labels[x] for x in best_path]
        # print(best_path)
        # print('path_score:', path_score.value())
        return best_path
