
def preprocess(file, out_file):
    prev_label = "O"
    with open(file, 'r', encoding='utf-8') as f,  open(out_file, 'w',encoding='utf-8') as fw:
        for line in f.readlines():
            line = line.rstrip()

            if line == "":
                # if prev_label == "-DOCSTART-":
                #     continue
                fw.write("\n")
                prev_label = "O"
                continue

            # print(line)
            # print(line)
            word, tag,label = line.split()

            # if word == "-DOCSTART-":
            #     f.readline() ##skip this
            #     prev_label = "-DOCSTART-"
            #     continue

            if prev_label == "O" and label.startswith("I-"):
                label= "B-" + label[2:]
            if prev_label.startswith("B") and label.startswith("I-"):
                if prev_label[2:] != label[2:]:
                    label = "B-" + label[2:]
            if prev_label.startswith("I") and label.startswith("I-"):
                if prev_label[2:] != label[2:]:
                    label = "B-" + label[2:]
            fw.write(word + " "+ tag +" " + label + "\n")

            prev_label = label



# preprocess("data/german/deu.train", "data/german/train.txt")
# preprocess("data/german/deu.testa", "data/german/dev.txt")
# preprocess("data/german/deu.testb", "data/german/test.txt")

# preprocess("data/dutch/deu.train.cv", "data/dutch/train.txt")
# preprocess("data/dutch/deu.testa.cv", "data/dutch/dev.txt")
# preprocess("data/dutch/deu.testb.cv", "data/dutch/test.txt")


# preprocess("data/spanish/train.unprocess.txt.", "data/spanish/train.txt")
# preprocess("data/spanish/dev.unprocess.txt", "data/spanish/dev.txt")
# preprocess("data/spanish/test.unprocess.txt", "data/spanish/test.txt")

preprocess("data/dutch/ned.train.", "data/dutch/train.txt")
preprocess("data/dutch/ned.testa", "data/dutch/dev.txt")
preprocess("data/dutch/ned.testb", "data/dutch/test.txt")