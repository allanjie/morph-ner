
import dynet_config

# dynet_config.set(mem=512, random_seed=42, autobatch=False)
import argparse
import random
import numpy as np
from config import Config
from reader import Reader
from baseline_lstmcrf import BiLSTM_CRF
from standard_lstmcrf import Standard_BiLSTM_CRF
import eval
from tqdm import tqdm
import math
import time
import os
import dynet as dy


def setSeed(seed):
    random.seed(seed)
    np.random.seed(seed)
    # dy_param.set_random_seed(seed)

def parse_arguments(parser):
    dynet_args = [
        "--dynet-mem",
        "--dynet-weight-decay",
        "--dynet-autobatch",
        "--dynet-gpus",
        "--dynet-gpu",
        "--dynet-devices",
        "--dynet-seed",
    ]
    for arg in dynet_args:
        parser.add_argument(arg)
    parser.add_argument('--mode', type=str, default='train', help="useless for now")
    # parser.add_argument('--gpu', action="store_true", default=False)
    parser.add_argument('--seed', type=int, default=1234, help="random seed")
    parser.add_argument('--digit2zero', action="store_true", default=True, help="convert digit to 0")

    parser.add_argument('--dataset',type=str, default="english")
    # parser.add_argument('--train_file', type=str, default="data/english/train.txt")
    # parser.add_argument('--dev_file', type=str, default="data/english/dev.txt")
    # parser.add_argument('--test_file', type=str, default="data/english/test.txt")
    # parser.add_argument('--embedding_file', type=str, default="data/glove.6B.100d.txt")
    # parser.add_argument('--embedding_file', type=str, default=None)
    parser.add_argument('--embedding_dim', type=int, default=100)
    parser.add_argument('--optimizer', type=str, default="adam")
    parser.add_argument('--learning_rate', type=float, default=0.1,help="sgd learning rate") ##only for sgd now
    parser.add_argument('--momentum', type=float, default=0.0)
    parser.add_argument('--l2', type=float, default=0.0)
    parser.add_argument('--batch_size', type=int, default=1)
    parser.add_argument('--num_epochs', type=int, default=50)

    ##model hyperparameter
    parser.add_argument('--model_type', type=str, default='standard',help="baseline or standard.")
    parser.add_argument('--hidden_dim', type=int, default=200)
    parser.add_argument('--char_emb_size', type=int, default=25)
    parser.add_argument('--charlstm_hidden_dim', type=int, default=50)
    parser.add_argument('--dropout', type=float, default=0.5)
    # parser.add_argument('--tanh_hidden_dim', type=int, default=100)
    parser.add_argument('--use_char_rnn', type=int, default=1, choices=[0,1])

    parser.add_argument('--train_num', type=int, default=-1)
    parser.add_argument('--dev_num', type=int, default=-1)
    parser.add_argument('--test_num', type=int, default=-1)
    parser.add_argument('--eval_freq', type=int, default=20000, help="frequency to evaluate ")
    parser.add_argument('--eval_epoch', type=int, default=0, help="evaluate dev set after a certain number")

    parser.add_argument('--load_param',type=int, choices=[0,1], default=0, help="load pretrained parameters")

    ##following is useless
    parser.add_argument('--use_hand_crafted', type=int, default=0,choices=[0,1], help="use hand crafted prefix")
    parser.add_argument('--concat_char_to_word', type=int, default=1, choices=[0,1],
                        help="concat the char embedding to word representation")
    parser.add_argument('--use_final_tanh', type=int, default=0, choices=[0, 1], help="use the final tanh layer or not") ## only used for benchmark main

    args = parser.parse_args()
    # dy.init()
    for k in args.__dict__:
        print(k + ": " + str(args.__dict__[k]))
    return args


def get_optimizer(model):

    if config.optimizer == "sgd":  ##good
        return dy.SimpleSGDTrainer(model, learning_rate=config.learning_rate)
    elif config.optimizer == "adam":
        return dy.AdamTrainer(model)
    elif config.optimizer == "adagrad":
        return dy.AdagradTrainer(model, learning_rate=config.learning_rate)
    elif config.optimizer == "rmsprop":
        return dy.RMSPropTrainer(model)
    elif config.optimizer == "momentum-sgd": ##good
        return dy.MomentumSGDTrainer(model, learning_rate=config.learning_rate)



def get_model(config, model, m_type):
    '''
    Choose different model based on the model type.
    :param config:
    :param model:
    :param m_type:
    :return:
    '''
    if m_type == "baseline":
        return BiLSTM_CRF(config, model)
    elif m_type == "standard":
        return Standard_BiLSTM_CRF(config, model, config.load_param) ##current model
    else:
        raise Exception("unknown type: " + m_type)


def train(epoch, insts, dev_insts, test_insts, model_type, batch_size = 1):

    model = dy.ParameterCollection()
    trainer = get_optimizer(model)

    bicrf = get_model(config, model, model_type)
    trainer.set_clip_threshold(5)
    print("number of instances: %d" % (len(insts)))

    best_dev = [-1, 0]
    best_test = [-1, 0]


    if not os.path.exists('models'):
        os.makedirs('models')

    model_name = "models/"+config.dataset+"."+model_type+"."+config.optimizer+"concat_"+str(config.concat_char_to_word)+".m"
    for i in range(epoch):
        epoch_loss = 0
        start_time = time.time()
        k = 0
        if batch_size != 1:
            index = 0
            while index < len(insts):
                mini_batch = insts[index: (index + batch_size)]
                dy.renew_cg()
                losses = []
                for inst in mini_batch:
                    input = inst.input.word_ids
                    # input = config.insert_singletons(inst.input.word_ids)
                    loss = bicrf.negative_log(input, inst.input.char_ids, inst.char_label_ids)
                    # loss_value = loss.value()
                    losses.append(loss)
                    # epoch_loss += loss_value
                    # print("finish one inst")
                # print("finish one batch")
                # loss = dy.esum(losses).scalar_value()
                index += batch_size
                loss = dy.average(losses)
                loss_value = loss.scalar_value()
                epoch_loss += loss_value
                loss.backward()
                trainer.update()

            k = k + 1

            if (i + 1) > config.eval_epoch:
                dev_metrics, test_metrics = evaluate(bicrf, dev_insts, test_insts)
                if dev_metrics[2] > best_dev[0]:
                    best_dev[0] = dev_metrics[2]
                    best_dev[1] = i
                if test_metrics[2] > best_test[0]:
                    best_test[0] = test_metrics[2]
                    best_test[1] = i
                k = 0

            end_time = time.time()
        else:
            for index in np.random.permutation(len(insts)):
                inst = insts[index]
                dy.renew_cg()
                input = inst.input.word_ids
                loss = bicrf.negative_log(input, inst.input.char_ids, inst.char_label_ids)
                loss_value = loss.value()
                loss.backward()
                trainer.update()
                epoch_loss += loss_value
                k = k + 1

                if (i+1) > config.eval_epoch and ( k % config.eval_freq == 0 or k == len(insts)) :
                    dev_metrics = evaluate(bicrf, "Dev", dev_insts)
                    test_metrics = evaluate(bicrf, "Test", test_insts)
                    if dev_metrics[2] > best_dev[0]:
                        best_dev[0] = dev_metrics[2]
                        best_dev[1] = i
                        model.save(model_name)
                    if test_metrics[2] > best_test[0]:
                        best_test[0] = test_metrics[2]
                        best_test[1] = i
                    k = 0
            end_time = time.time()
        print("Epoch %d: %.5f, Time is %.2fs" % (i + 1, epoch_loss, end_time-start_time), flush=True)
    print("The best dev: %.2f" % (best_dev[0]))
    print("The best test: %.2f" % (best_test[0]))
    model.populate(model_name)
    evaluate(bicrf, "Test", test_insts)

def evaluate(model, name, insts):
    for inst in insts:
        dy.renew_cg()
        inst.prediction = model.decode(inst.input.word_ids, inst.input.char_ids)
    test_metrics = eval.evaluate_character_model(insts)
    print("[%s set] Precision: %.2f, Recall: %.2f, F1: %.2f" % (name, test_metrics[0], test_metrics[1], test_metrics[2]))
    return test_metrics

def test(test_insts, model_type):
    model = dy.ParameterCollection()
    bicrf = get_model(config, model, model_type)
    model_name = "models/" + config.dataset + "." + model_type + "." + config.optimizer + "concat_" + str(
        config.concat_char_to_word) + ".m"
    model.populate(model_name)
    evaluate(bicrf, "Test", test_insts)
    with open("results/"+config.dataset+".res", 'w', encoding='utf-8') as f:

        for inst in test_insts:
            for word, label, predict_label in zip(inst.input.words, inst.output, inst.prediction):
                f.write(word + " " + label + " " + predict_label + "\n")
            f.write("\n")




if __name__ == "__main__":



    parser = argparse.ArgumentParser(description="LSTM CRF implementation")
    opt = parse_arguments(parser)

    config = Config(opt)


    reader = Reader(config.digit2zero, config.dataset)
    setSeed(config.seed)

    train_insts = reader.read_from_file(config.train_file, config.train_num, True)
    dev_insts = reader.read_from_file(config.dev_file, config.dev_num, False)
    test_insts = reader.read_from_file(config.test_file, config.test_num, False)

    config.use_iobes(train_insts)
    config.use_iobes(dev_insts)
    config.use_iobes(test_insts)
    config.build_label_idx(train_insts)
    # print("All vocabulary")
    # print(reader.all_vocab)



    config.build_emb_table(reader.train_vocab, reader.test_vocab)

    config.find_singleton(train_insts)
    config.map_insts_ids(train_insts)
    config.map_insts_ids(dev_insts)
    config.map_insts_ids(test_insts)



    print("num chars: " + str(config.num_char))
    # print(str(config.char2idx))

    print("num words: " + str(len(config.word2idx)))
    # print(config.word2idx)
    if opt.mode == "train":
        train(config.num_epochs, train_insts, dev_insts, test_insts, config.model_type, config.batch_size)
    else:
        test(test_insts, config.model_type)

    print(opt.mode)