# 
# @author: Allan
#

from tqdm import tqdm
from common.sentence import Sentence
from common.instance import Instance
import re


class Reader:


    def __init__(self, digit2zero, dataset):
        self.digit2zero = digit2zero
        self.dataset = dataset
        self.train_vocab = []
        self.test_vocab = []



    def read_from_file(self, file, number=-1, is_train=True):
        print("Reading file: " + file)
        insts = []
        # vocab = set() ## build the vocabulary
        max_word_len = -1
        max_sent_len = -1
        with open(file, 'r', encoding='utf-8') as f:
            words = []
            labels = []
            for line in tqdm(f.readlines()):
                line = line.rstrip()
                if line == "":

                    ## This `if` handles some errors caused by
                    ## the German language. You can delete this if
                    ## you are only working on english
                    ## OR delete it accordingly based on your data
                    if len(words) == 1:
                        pattern = re.compile("^(-)+$")
                        res = pattern.match(words[0])
                        if res:
                            words = []
                            labels = []
                            continue
                    max_sent_len = max (len(words), max_sent_len)

                    for word in words:
                        max_word_len = max(max_word_len, len(word))
                    insts.append(Instance(Sentence(words), labels))
                    words = []
                    labels = []
                    if len(insts) == number:
                        break
                    continue
                if self.dataset == "spanish":
                    word, label = line.split()
                else:
                    word, _, label = line.split()
                if self.digit2zero:
                    word = re.sub('\d', '0', word)
                # max_word_len = max(max_word_len, len(word))
                words.append(word)
                if is_train:
                    if word not in self.train_vocab:
                        self.train_vocab.append(word)
                else:
                    if word not in self.test_vocab:
                        self.test_vocab.append(word)
                labels.append(label)

        print("max word len: " + str(max_word_len))
        print("max sent len: " + str(max_sent_len))
        return insts
