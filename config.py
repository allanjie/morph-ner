# 
# @author: Allan
#

import numpy as np
from tqdm import tqdm


class Config:
    def __init__(self, args):

        self.PAD = "<PAD>"
        self.B = "B-"
        self.I = "I-"
        self.S = "S-"
        self.E = "E-"
        self.O = "O"
        self.START_TAG = "<START>"
        self.STOP_TAG = "<STOP>"
        self.unk = "</s>"
        self.unk_id = -1



        # self.device = torch.device("cuda" if args.gpu else "cpu")
        self.dataset = args.dataset
        self.train_file = "data/"+self.dataset+"/train.txt"
        self.dev_file = "data/"+self.dataset+"/dev.txt"
        self.test_file = "data/"+self.dataset+"/test.txt"
        print("train_file: %s\ndev_file: %s\ntest_file: %s" % (self.train_file, self.dev_file, self.test_file))

        if self.dataset == "spanish":
            self.unk = "unk"

        self.embedding_file = "data/"+self.dataset+".embeddings.txt"
        # self.embedding_file = None  ## set to None for debug purpose
        self.embedding_dim = args.embedding_dim
        self.embedding, self.embedding_dim = self.read_pretrain_embedding()
        self.word_embedding = None
        self.seed = args.seed
        self.digit2zero = args.digit2zero

        self.label2idx = {}
        self.idx2labels = []
        self.ent2idx = {}
        self.idx2entity = []
        self.label_idx2ent_idx = []
        self.char2idx = {}
        self.idx2char = []
        self.num_char = 0


        self.optimizer = args.optimizer.lower()
        self.learning_rate = args.learning_rate
        self.momentum = args.momentum
        self.l2 = args.l2
        self.num_epochs = args.num_epochs
        # self.lr_decay = 0.05
        self.train_num = args.train_num
        self.dev_num = args.dev_num
        self.test_num = args.test_num
        self.batch_size = args.batch_size
        self.eval_freq = args.eval_freq
        self.eval_epoch = args.eval_epoch

        self.model_type = args.model_type
        self.hidden_dim = args.hidden_dim

        self.use_final_tanh = args.use_final_tanh
        self.tanh_hidden_dim = self.hidden_dim // 2
        self.use_brnn = True
        self.num_layers = 1
        self.dropout = args.dropout
        self.char_emb_size = args.char_emb_size
        self.charlstm_hidden_dim = args.charlstm_hidden_dim
        self.use_char_rnn = args.use_char_rnn

        self.use_prefix_hf = args.use_hand_crafted

        self.load_param = args.load_param
        self.concat_char_to_word = args.concat_char_to_word

    '''
      read all the  pretrain embeddings
    '''
    def read_pretrain_embedding(self):
        print("reading the pretraed embedding: %s" % (self.embedding_file))
        if self.embedding_file is None:
            print("pretrain embedding in None, using random embedding")
            return None, self.embedding_dim
        embedding_dim = -1
        embedding = dict()
        skip_invalid = 0
        with open(self.embedding_file, 'r', encoding='utf-8') as file:
            for line in tqdm(file.readlines()):
                line = line.strip()
                if len(line) == 0:
                    continue
                tokens = line.split()
                if len(tokens) == 2:
                    continue
                if embedding_dim < 0:
                    embedding_dim = len(tokens) - 1
                else:
                    # print(tokens)
                    # print(embeddtanh_hidden_diming_dim)
                    if embedding_dim + 1 != len(tokens):
                        # print(line)
                        # assert (embedding_dim + 1 == len(tokens))
                        skip_invalid +=1
                        continue
                    # assert (embedding_dim + 1 == len(tokens))
                embedd = np.empty([1, embedding_dim])
                embedd[:] = tokens[1:]
                first_col = tokens[0]
                embedding[first_col] = embedd
        print("Invalid embedding skipped:", skip_invalid)
        return embedding, embedding_dim


    '''
        build the embedding table
        obtain the word2idx and idx2word as well.
    '''
    def build_emb_table(self, train_vocab, test_vocab):
        print("Building the embedding table for vocabulary...")
        scale = np.sqrt(3.0 / self.embedding_dim)

        self.word2idx = dict()
        self.idx2word = []
        self.word2idx[self.unk] = 0
        self.unk_id = 0
        self.idx2word.append(self.unk)

        self.char2idx[self.unk] = 0
        self.idx2char.append(self.unk)

        for word in train_vocab:
            self.word2idx[word] = len(self.word2idx)
            self.idx2word.append(word)
            for c in word:
                if c not in self.char2idx:
                    self.char2idx[c] = len(self.idx2char)
                    self.idx2char.append(c)

        for word in test_vocab:
            if word not in self.word2idx:
                self.word2idx[word] = len(self.word2idx)
                self.idx2word.append(word)
                for c in word:
                    if c not in self.char2idx:
                        self.char2idx[c] = len(self.idx2char)
                        self.idx2char.append(c)
        self.num_char = len(self.idx2char)
        # print(self.word2idx)
        # print(self.char2idx) #sometimes print dutch/spanish/german characther will have error

        if self.embedding is not None:
            print("[Info] Use the pretrained word embedding to initialize: %d x %d" % (len(self.word2idx), self.embedding_dim))
            self.word_embedding = np.empty([len(self.word2idx), self.embedding_dim])
            for word in self.word2idx:
                if word in self.embedding:
                    self.word_embedding[self.word2idx[word], :] = self.embedding[word]
                elif word.lower() in self.embedding:
                    self.word_embedding[self.word2idx[word], :] = self.embedding[word.lower()]
                else:
                    self.word_embedding[self.word2idx[word], :] = self.embedding[self.unk]
                    # self.word_embedding[self.word2idx[word], :] = np.random.uniform(-scale, scale, [1, self.embedding_dim])
            self.embedding = None
        else:
            self.word_embedding = np.empty([len(self.word2idx), self.embedding_dim])
            for word in self.word2idx:
                self.word_embedding[self.word2idx[word], :] = np.random.uniform(-scale, scale, [1, self.embedding_dim])


    def build_label_idx(self, insts):
        for inst in insts:
            for entity in inst.output:
                if entity not in self.ent2idx:
                    self.idx2entity.append(entity)
                    self.ent2idx[entity] = len(self.ent2idx)
        self.ent2idx[self.START_TAG] = len(self.ent2idx)
        self.idx2entity.append(self.START_TAG)
        self.ent2idx[self.STOP_TAG] = len(self.ent2idx)
        self.idx2entity.append(self.STOP_TAG)
        print("#entity : " + str(len(self.ent2idx)))
        print("entity 2idx: " + str(self.ent2idx))

        for inst in insts:
            for char_labels in inst.all_possible_tags:
                for label in char_labels:
                    if label not in self.label2idx:
                        self.idx2labels.append(label)
                        self.label2idx[label] = len(self.label2idx)

        self.label2idx[self.START_TAG] = len(self.label2idx)
        self.idx2labels.append(self.START_TAG)
        self.label2idx[self.STOP_TAG] = len(self.label2idx)
        self.idx2labels.append(self.STOP_TAG)
        self.label_size = len(self.label2idx)
        print("#labels: " + str(self.label_size))
        print("label 2idx: " + str(self.label2idx))

        for label_idx in range(len(self.idx2labels)):
            label = self.idx2labels[label_idx]
            if label == self.O or label == self.START_TAG or label == self.STOP_TAG:
                self.label_idx2ent_idx.append(self.ent2idx[label])
            else:
                self.label_idx2ent_idx.append(self.ent2idx[label[:-2]])
        print("label idx to entity idx")
        print(self.label_idx2ent_idx)


    def use_iobes(self, insts):
        for inst in insts:
            output = inst.output
            for pos in range(len(output)):
                # print(pos)
                curr_entity = output[pos]
                char_possible = set()
                if pos == len(inst) - 1:
                    if curr_entity.startswith(self.B):
                        output[pos] = curr_entity.replace(self.B, self.S)
                    elif curr_entity.startswith(self.I):
                        output[pos] = curr_entity.replace(self.I, self.E)
                else:
                    next_entity = output[pos + 1]
                    if curr_entity.startswith(self.B):
                        if next_entity.startswith(self.O) or next_entity.startswith(self.B):
                            output[pos] = curr_entity.replace(self.B, self.S)
                    elif curr_entity.startswith(self.I):
                        if next_entity.startswith(self.O) or next_entity.startswith(self.B):
                            output[pos] = curr_entity.replace(self.I, self.E)
            all_char_possible_labels = []
            # print(output)
            for pos in range(len(output)):
                char_possible = {}
                if output[pos] != self.O:
                    # print(output[pos])
                    char_possible[output[pos] + "-B"]=0
                    char_possible[output[pos] + "-I"]=0
                    char_possible[output[pos] + "-E"]=0
                    char_possible[output[pos] + "-S"]=0
                else:
                    char_possible[self.O]=0
                # print("pos: " +str(pos) + " "+ str(char_possible))
                all_char_possible_labels.append(char_possible)
            # print("END first printing")
            inst.all_possible_tags = all_char_possible_labels

    def map_insts_ids(self, insts):
        for inst in insts:
            words = inst.input.words
            inst.input.word_ids = []
            inst.input.char_ids = []
            inst.char_label_ids = []
            for word in words:
                if word in self.word2idx:
                    inst.input.word_ids.append(self.word2idx[word])
                else:
                    inst.input.word_ids.append(self.word2idx[self.unk])
                char_id = []
                for c in word:
                    if c in self.char2idx:
                        char_id.append(self.char2idx[c])
                    else:
                        char_id.append(self.char2idx[self.unk])
                inst.input.char_ids.append(char_id)
            for char_labels in inst.all_possible_tags:
                curr = set()
                for label in char_labels:
                    curr.add(self.label2idx[label])
                inst.char_label_ids.append(curr)
        # return insts_ids


    # def map_word_to_ids_in_insts(self, insts):
    #     for inst in insts:
    #         words = inst.input.words
    #         inst.input_ids = []
    #         for word in words:
    #             inst.input_ids.append(self.word2idx[word])

    def find_singleton(self, train_insts):
        freq = {}
        self.singleton = {}
        for inst in train_insts:
            words = inst.input.words
            for w in words:
                if w in freq:
                    freq[w] += 1
                else:
                    freq[w] = 1
        for w in freq:
            if freq[w] == 1:
                self.singleton[self.word2idx[w]] = 0

    def insert_singletons(self, words, p=0.5):
        """
        Replace singletons by the unknown word with a probability p.
        """
        new_words = []
        for word in words:
            if word in self.singleton and np.random.uniform() < p:
                new_words.append(self.unk_id)
            else:
                new_words.append(word)
        return new_words



    def extract_all_prefix(self, train_insts):
        '''
        Hand-crafted features.
        :param train_insts:
        :return:
        '''
        self.prefix_dict = {}
        ##len = 1, 2, 3, 4, 5
        for inst in train_insts:
            for word in inst.input.words:
                for length in range(1,6):
                    if word[:length] not in self.prefix_dict:
                        self.prefix_dict[word[:length]] = len(self.prefix_dict)
        print("number of unique prefix : %d" % (len(self.prefix_dict)))

    def get_prefix_feat_vec(self, insts):
        '''
        Create hand-crafted feature vector for each instance
        :param insts:
        :return:
        '''
        vec = np.zeros(len(self.prefix_dict))
        for inst in insts:
            for word in inst.input.words:
                for length in range(1,6):
                    if word[:length] in self.prefix_dict:
                        vec[self.prefix_dict[word[:length]]] += 1
            inst.prefix_vec = vec