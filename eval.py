

class Span:

    def __init__(self, left, right, type):
        self.left = left
        self.right = right
        self.type = type

    def __eq__(self, other):
        return self.left == other.left and self.right == other.right and self.type == other.type

    def __hash__(self):
        return hash((self.left, self.right, self.type))


def evaluate_word_model(insts):
    p = 0
    total_entity = 0
    total_predict = 0

    for inst in insts:

        output = inst.output
        prediction = inst.prediction
        # convert to span
        output_spans = set()
        start = -1
        for i in range(len(output)):
            if output[i].startswith("B-"):
                start = i
            if output[i].startswith("E-"):
                end = i
                output_spans.add(Span(start, end, output[i][2:]))
            if output[i].startswith("S-"):
                output_spans.add(Span(i, i, output[i][2:]))
        predict_spans = set()
        for i in range(len(prediction)):
            if prediction[i].startswith("B-"):
                start = i
            if prediction[i].startswith("E-"):
                end = i
                predict_spans.add(Span(start, end, prediction[i][2:]))
            if prediction[i].startswith("S-"):
                predict_spans.add(Span(i, i, prediction[i][2:]))

        total_entity += len(output_spans)
        total_predict += len(predict_spans)
        p += len(predict_spans.intersection(output_spans))

    precision = p * 1.0 / total_predict * 100 if total_predict != 0 else 0
    recall = p * 1.0 / total_entity * 100 if total_entity != 0 else 0
    fscore = 2.0 * precision * recall / (precision + recall) if precision != 0 or recall != 0 else 0

    return [precision, recall, fscore]

## the input to the evaluation should already have
## have the predictions which is the label.
## iobest tagging scheme
def evaluate_character_model(insts):
    '''
    Evaluation for the character-level model
    :param insts:
    :return:
    '''
    p = 0
    total_entity = 0
    total_predict = 0

    for inst in insts:

        output = inst.output
        prediction = inst.prediction
        # print(output)
        # print(prediction)
        pred_word_label = []
        curr_word_id = 0
        curr_word_len = len(inst.input.words[curr_word_id])
        k = 0
        for i in range(len(prediction)):
            if k == 0: ## start of a word, take the label of the first word
                pred_word_label.append(prediction[i][:-2] if prediction[i] != "O" else "O")
                if curr_word_id == len(inst.input.words) - 1:
                    break
            k += 1
            if k == curr_word_len:
                k = 0
                curr_word_id += 1
                curr_word_len = len(inst.input.words[curr_word_id])

        # print(pred_word_label)
        #convert to span
        output_spans = set()
        start = -1
        for i in range(len(output)):
            if output[i].startswith("B-"):
                start = i
            if output[i].startswith("E-"):
                end = i
                output_spans.add(Span(start, end, output[i][2:]))
            if output[i].startswith("S-"):
                output_spans.add(Span(i, i, output[i][2:]))
        # print(output_spans)
        predict_spans = set()
        for i in range(len(pred_word_label)):
            if pred_word_label[i].startswith("B-"):
                start = i
            if pred_word_label[i].startswith("E-"):
                end = i
                predict_spans.add(Span(start, end, pred_word_label[i][2:]))
            if pred_word_label[i].startswith("S-"):
                predict_spans.add(Span(i, i, pred_word_label[i][2:]))

        total_entity += len(output_spans)
        total_predict += len(predict_spans)
        p += len(predict_spans.intersection(output_spans))

    precision = p * 1.0 / total_predict * 100 if total_predict != 0 else 0
    recall = p * 1.0 / total_entity * 100 if total_entity != 0 else 0
    fscore = 2.0 * precision * recall / (precision + recall) if precision != 0 or recall != 0 else 0

    return [precision, recall, fscore]
