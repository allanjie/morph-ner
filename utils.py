import numpy as np
import dynet as dy
from config import Config

def log_sum_exp(scores, num_labels):
    # return dy.logsumexp(scores)
    # npval = scores.npvalue()
    # print("curre")
    # print(npval)
    # print("after")
    # # argmax_score = np.argmax(npval)
    # # argmax_score = dy.argmax(scores, gradient_mode="straight_through_gradient")
    # argmax_score = dy.argmax(scores, gradient_mode="zero_gradient")
    #
    # # max_score_expr = dy.pick(scores, argmax_score)
    # max_score_expr = dy.dot_product(scores, argmax_score)
    max_score_expr = dy.max_dim(scores)
    max_score_expr_broadcast = dy.concatenate([max_score_expr] * num_labels)
    # return max_score_expr + dy.log(dy.sum_cols(dy.transpose(dy.exp(scores - max_score_expr_broadcast))))
    '''
    sum_cols(x) has been deprecated.
    Please use sum_dim(x, [1]) instead.
    '''
    return max_score_expr + dy.log(dy.sum_dim(dy.exp(scores - max_score_expr_broadcast), [0]))

def check_bies_constraint(config:Config, previous: str, next: str) -> bool:
    if previous == config.B[0] or previous == config.I[0]:
        return next == config.I[0] or next == config.E[0]
    elif previous == config.E[0] or previous == config.S[0]:
        return next == config.S[0] or next == config.B[0]
    else:
        raise Exception("unkown previous label: " + previous)


def check_word_level_bies_constraint(previous: str, next: str) -> bool:
    previous_entity = previous[:-2] if len(previous) > 2 else previous
    next_entity = next[:-2] if len(next) > 2 else next
    # print(previous_entity + " " + next_entity)
    previous_suffix = previous[-1] if len(previous) > 2 else previous
    next_suffix = next[-1] if len(next) > 2 else next
    # print(previous_suffix + " " + next_suffix)
    if previous_suffix == "B" or previous_suffix == "I" or next_suffix == "I" or next_suffix=="E":
        # print(previous + " " + next)
        return False
    if previous_entity.startswith("B-") or previous_entity.startswith("I-"):
        if next_entity.startswith("I-") or next_entity.startswith("E-"):
            return previous_entity[2:] == next_entity[2:]
        else:
            return False
    elif previous_entity.startswith("S-") or previous_entity.startswith("E-") or previous_entity == "O":
        return next_entity.startswith("B-") or next_entity.startswith("S-") or next_entity == "O"
    else:
        raise Exception("unkown previous type: %s" % (previous_entity))
    pass
